import string


def read_number(prompt="Please enter a number: "):
    if prompt != "" and prompt[-1] not in string.whitespace:
        prompt = prompt + " "
    while True:
        result = input(prompt)
        try:
            num = int(result)
            return num
        except ValueError:
            try:
                num = float(result)
                return num
            except ValueError:
                print("But that wasn't a number!")


def read_yesorno(prompt="Yes or no? "):
    if prompt != "" and prompt[-1] not in string.whitespace:
        prompt += " "

    while True:
        result = input(prompt)
        if result.lower() in ["y", "yes"]:
            return True
        elif result.lower() in ["n", "no"]:
            return False
        print("Please answer yes or no.\n")
