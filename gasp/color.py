#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This program is part of GASP, a toolkit for newbie Python Programmers.
# Copyright 2020 (C) the GASP Development Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Improved GASP Color System
# Developed by Richard Martinez


class Color:
    """
    Color container class that supports both RGB and HEX. All method calls are NOT case-sensitive.
    """

    def __init__(self, mode):
        self.set_mode(mode)

    def __getattr__(self, item):
        """
        Returns stored color value in correct format.
        """
        try:
            return getattr(self.storage, str(item).upper())
        except AttributeError:
            raise ValueError("Please enter a supported color.")

    def set_mode(self, mode):
        """
        Sets mode between RGB and HEX. Pass argument as a string.
        """
        mode = mode.upper().strip()

        if mode == "RGB":
            self.storage = RGB()
        elif mode == "HEX":
            self.storage = HEX()
        else:
            raise ValueError("The two acceptable modes are 'RGB' and 'HEX'")

    def available(self):
        """
        Returns a list of all available colors.
        """
        lst = []

        for attr in dir(self.storage):
            if (not attr.startswith("__")) and attr == attr.upper():

                if isinstance(
                    getattr(self, attr), tuple
                ):  # checks is the color a hex value?
                    color_val = ", ".join(map(str, getattr(self, attr)))

                else:  # is RGB
                    color_val = getattr(self, attr)

                color_entry = [attr, color_val]

                formatted_color = ": ".join(color_entry)
                lst.append(formatted_color)
        return lst

    @staticmethod
    def hex_to_rgb(hex_string):
        """
        Converts a HEX string into an RGB three tuple. Leading # is optional.
        """
        try:
            if hex_string.startswith("#"):
                hex_string = hex_string[1:]
        except TypeError:
            raise TypeError("Input must be a string")

        return tuple(int(hex_string[i : i + 2], 16) for i in (0, 2, 4))

    @staticmethod
    def rgb_to_hex(*args):
        """
        Converts RGB values to a HEX string.
        Designed to be error proof: Can pass either as a three tuple, or exactly three passed parameters.
        """
        try:
            if len(args) == 1:
                args = tuple(n for n in args[0])
            return "#%02X%02X%02X" % args

        except Exception:
            raise TypeError(
                "Input must be a tuple. Either a single passed tuple, or exactly three passed params."
            )

    def get_rgb_and_hex(self, color_name):
        color_name = color_name.upper().strip()

        if color_name in dir(self.storage):
            first_val = getattr(self, color_name)

            if first_val[0] == "#":  # starting with hex
                second_val = color.hex_to_rgb(first_val)
            else:  # starting with rgb
                second_val = color.rgb_to_hex(first_val)

            return color_name, first_val, second_val

        else:
            raise ValueError(
                "The inputted color name is not part of the list of colors. "
                "Use color.available() to see the list"
            )


class RGB:
    """Constant container for RGB values."""

    ALICEBLUE = (240, 248, 255)
    ANTIQUEWHITE = (250, 235, 215)
    AQUA = (0, 255, 255)
    AZURE = (240, 255, 212)
    BEIGE = (245, 245, 220)
    BISQUE = (245, 245, 220)
    BLACK = (0, 0, 0)
    BLANCHEDALMOND = (255, 255, 205)
    BLUE = (0, 0, 255)
    BLUEVIOLET = (138, 43, 226)
    BROWN = (165, 42, 42)
    BURLYWOOD = (222, 184, 135)
    CADETBLUE = (95, 158, 160)
    CHARTREUSE = (127, 255, 0)
    CHOCOLATE = (210, 105, 30)
    CORAL = (255, 127, 80)
    CORNFLOWERBLUE = (100, 149, 237)
    CORNSILK = (255, 248, 220)
    CRIMSON = (220, 20, 60)
    CYAN = (0, 255, 255)
    DARKBLUE = (0, 0, 139)
    DARKCYAN = (0, 139, 139)
    DARKGOLDENROD = (184, 134, 11)
    DARKGRAY = (169, 169, 169)
    DARKGREEN = (0, 100, 0)
    DARKKHAKI = (189, 183, 107)
    DARKMAGENTA = (139, 0, 139)
    DARKOLIVEGREEN = (85, 107, 47)
    DARKORANGE = (255, 140, 0)
    DARKORCHID = (155, 50, 204)
    DARKRED = (139, 0, 0)
    DARKSALMON = (233, 150, 122)
    DARKSEAGREEN = (143, 188, 143)
    DARKSLATEBLUE = (72, 61, 139)
    DARKSLATEGRAY = (47, 79, 79)
    DARKTURQUOISE = (0, 206, 209)
    DARKVIOLET = (148, 0, 211)
    DEEPPINK = (255, 20, 147)
    DEEPSKYBLUE = (0, 191, 255)
    DIMGRAY = (105, 105, 105)
    DODGERBLUE = (30, 144, 255)
    FIREBRICK = (178, 34, 34)
    FLORALWHITE = (255, 250, 240)
    FORESTGREEN = (34, 139, 34)
    FUCHSIA = (255, 0, 255)
    GAINSBORO = (220, 220, 220)
    GHOSTWHITE = (248, 248, 255)
    GOLD = (255, 215, 0)
    GOLDENROD = (218, 165, 32)
    GRAY = (127, 127, 127)
    GREEN = (0, 128, 0)
    GREENYELLOW = (173, 255, 47)
    HONEYDEW = (240, 255, 240)
    HOTPINK = (255, 105, 180)
    INDIANRED = (205, 92, 92)
    INDIGO = (75, 0, 130)
    IVORY = (255, 240, 240)
    KHAKI = (240, 230, 140)
    LAVENDER = (230, 230, 250)
    LAVENDERBLUSH = (255, 240, 245)
    LAWNGREEN = (124, 252, 245)
    LEMONCHIFFON = (255, 250, 205)
    LIGHTBLUE = (173, 216, 230)
    LIGHTCORAL = (240, 128, 128)
    LIGHTCYAN = (224, 255, 255)
    LIGHTGOLDENRODYELLOW = (250, 250, 210)
    LIGHTGREEN = (144, 238, 144)
    LIGHTGRAY = (211, 211, 211)
    LIGHTPINK = (255, 182, 193)
    LIGHTSALMON = (255, 160, 122)
    LIGHTSEAGREEN = (32, 178, 170)
    LIGHTSKYBLUE = (135, 206, 250)
    LIGHTSLATEGRAY = (119, 136, 153)
    LIGHTSTEELBLUE = (176, 196, 222)
    LIGHTYELLOW = (255, 255, 224)
    LIME = (0, 255, 0)
    LIMEGREEN = (50, 205, 50)
    LINEN = (250, 240, 230)
    MAGENTA = (255, 0, 255)
    MAROON = (128, 0, 0)
    MEDIUMAQUAMARINE = (102, 205, 170)
    MEDIUMBLUE = (0, 0, 205)
    MEDIUMORCHID = (186, 85, 211)
    MEDIUMPURPLE = (147, 112, 219)
    MEDIUMSEAGREEN = (60, 179, 113)
    MEDIUMSLATEBLUE = (123, 104, 238)
    MEDIUMSPRINGGREEN = (0, 250, 154)
    MEDIUMTURQOISE = (72, 209, 204)
    MEDIUMVIOLETRED = (199, 21, 133)
    MIDNIGHTBLUE = (25, 25, 112)
    MINTCREAM = (245, 255, 250)
    MISTYROSE = (255, 228, 225)
    MOCCASIN = (255, 225, 181)
    NAVAJOWHITE = (255, 222, 173)
    NAVY = (0, 0, 128)
    OLDLACE = (253, 245, 230)
    OLIVE = (128, 128, 0)
    OLIVEDRAB = (107, 142, 35)
    ORANGE = (255, 165, 0)
    ORANGERED = (255, 69, 0)
    ORCHID = (218, 122, 214)
    PALEGOLDENROD = (238, 232, 170)
    PALEGREEN = (152, 251, 152)
    PALETURQOISE = (175, 238, 238)
    PALEVIOLETRED = (219, 112, 147)
    PAPAYAWHIP = (255, 239, 213)
    PEACHPUFF = (255, 239, 213)
    PERU = (205, 133, 63)
    PINK = (255, 192, 203)
    PLUM = (211, 160, 221)
    POWDERBLUE = (176, 224, 230)
    PURPLE = (128, 0, 128)
    RED = (255, 0, 0)
    ROSYBROWN = (188, 143, 143)
    ROYALBLUE = (65, 105, 225)
    SADDLEBROWN = (139, 69, 19)
    SALMON = (250, 128, 114)
    SANDYBROWN = (244, 164, 96)
    SEAGREEN = (46, 139, 87)
    SEASHELL = (255, 245, 238)
    SIENNA = (160, 82, 45)
    SILVER = (192, 192, 192)
    SKYBLUE = (135, 206, 235)
    SLATEBLUE = (106, 90, 205)
    SLATEGRAY = (112, 128, 144)
    SNOW = (255, 250, 250)
    SPRINGGREEN = (0, 255, 127)
    STEELBLUE = (70, 130, 180)
    TAN = (210, 180, 140)
    TEAL = (0, 128, 128)
    THISTLE = (216, 191, 216)
    TOMATO = (253, 99, 71)
    TURQUOISE = (64, 224, 208)
    VIOLET = (238, 130, 238)
    WHEAT = (245, 222, 170)
    WHITE = (255, 255, 255)
    WHITESMOKE = (245, 245, 245)
    YELLOW = (255, 255, 0)
    YELLOWGREEN = (154, 205, 50)


class HEX:
    """Constant container for HEX values."""

    ALICEBLUE = "#F0F8FF"
    ANTIQUEWHITE = "#FAEBD7"
    AQUA = "#00FFFF"
    AZURE = "#F0FFD4"
    BEIGE = "#F5F5DC"
    BISQUE = "#F5F5DC"
    BLACK = "#000000"
    BLANCHEDALMOND = "#FFFFCD"
    BLUE = "#0000FF"
    BLUEVIOLET = "#8A2BE2"
    BROWN = "#A52A2A"
    BURLYWOOD = "#DEB887"
    CADETBLUE = "#5F9EA0"
    CHARTREUSE = "#7FFF00"
    CHOCOLATE = "#D2691E"
    CORAL = "#FF7F50"
    CORNFLOWERBLUE = "#6495ED"
    CORNSILK = "#FFF8DC"
    CRIMSON = "#DC143C"
    CYAN = "#00FFFF"
    DARKBLUE = "#00008B"
    DARKCYAN = "#008B8B"
    DARKGOLDENROD = "#B8860B"
    DARKGRAY = "#A9A9A9"
    DARKGREEN = "#006400"
    DARKKHAKI = "#BDB76B"
    DARKMAGENTA = "#8B008B"
    DARKOLIVEGREEN = "#556B2F"
    DARKORANGE = "#FF8C00"
    DARKORCHID = "#9B32CC"
    DARKRED = "#8B0000"
    DARKSALMON = "#E9967A"
    DARKSEAGREEN = "#8FBC8F"
    DARKSLATEBLUE = "#483D8B"
    DARKSLATEGRAY = "#2F4F4F"
    DARKTURQUOISE = "#00CED1"
    DARKVIOLET = "#9400D3"
    DEEPPINK = "#FF1493"
    DEEPSKYBLUE = "#00BFFF"
    DIMGRAY = "#696969"
    DODGERBLUE = "#1E90FF"
    FIREBRICK = "#B22222"
    FLORALWHITE = "#FFFAF0"
    FORESTGREEN = "#228B22"
    FUCHSIA = "#FF00FF"
    GAINSBORO = "#DCDCDC"
    GHOSTWHITE = "#F8F8FF"
    GOLD = "#FFD700"
    GOLDENROD = "#DAA520"
    GRAY = "#7F7F7F"
    GREEN = "#008000"
    GREENYELLOW = "#ADFF2F"
    HONEYDEW = "#F0FFF0"
    HOTPINK = "#FF69B4"
    INDIANRED = "#CD5C5C"
    INDIGO = "#4B0082"
    IVORY = "#FFF0F0"
    KHAKI = "#F0E68C"
    LAVENDER = "#E6E6FA"
    LAVENDERBLUSH = "#FFF0F5"
    LAWNGREEN = "#7CFCF5"
    LEMONCHIFFON = "#FFFACD"
    LIGHTBLUE = "#ADD8E6"
    LIGHTCORAL = "#F08080"
    LIGHTCYAN = "#E0FFFF"
    LIGHTGOLDENRODYELLOW = "#FAFAD2"
    LIGHTGREEN = "#90EE90"
    LIGHTGRAY = "#D3D3D3"
    LIGHTPINK = "#FFB6C1"
    LIGHTSALMON = "#FFA07A"
    LIGHTSEAGREEN = "#20B2AA"
    LIGHTSKYBLUE = "#87CEFA"
    LIGHTSLATEGRAY = "#778899"
    LIGHTSTEELBLUE = "#B0C4DE"
    LIGHTYELLOW = "#FFFFE0"
    LIME = "#00FF00"
    LIMEGREEN = "#32CD32"
    LINEN = "#FAF0E6"
    MAGENTA = "#FF00FF"
    MAROON = "#800000"
    MEDIUMAQUAMARINE = "#66CDAA"
    MEDIUMBLUE = "#0000CD"
    MEDIUMORCHID = "#BA55D3"
    MEDIUMPURPLE = "#9370DB"
    MEDIUMSEAGREEN = "#3CB371"
    MEDIUMSLATEBLUE = "#7B68EE"
    MEDIUMSPRINGGREEN = "#00FA9A"
    MEDIUMTURQOISE = "#48D1CC"
    MEDIUMVIOLETRED = "#C71585"
    MIDNIGHTBLUE = "#191970"
    MINTCREAM = "#F5FFFA"
    MISTYROSE = "#FFE4E1"
    MOCCASIN = "#FFE1B5"
    NAVAJOWHITE = "#FFDEAD"
    NAVY = "#000080"
    OLDLACE = "#FDF5E6"
    OLIVE = "#808000"
    OLIVEDRAB = "#6B8E23"
    ORANGE = "#FFA500"
    ORANGERED = "#FF4500"
    ORCHID = "#DA7AD6"
    PALEGOLDENROD = "#EEE8AA"
    PALEGREEN = "#98FB98"
    PALETURQOISE = "#AFEEEE"
    PALEVIOLETRED = "#DB7093"
    PAPAYAWHIP = "#FFEFD5"
    PEACHPUFF = "#FFEFD5"
    PERU = "#CD853F"
    PINK = "#FFC0CB"
    PLUM = "#D3A0DD"
    POWDERBLUE = "#B0E0E6"
    PURPLE = "#800080"
    RED = "#FF0000"
    ROSYBROWN = "#BC8F8F"
    ROYALBLUE = "#4169E1"
    SADDLEBROWN = "#8B4513"
    SALMON = "#FA8072"
    SANDYBROWN = "#F4A460"
    SEAGREEN = "#2E8B57"
    SEASHELL = "#FFF5EE"
    SIENNA = "#A0522D"
    SILVER = "#C0C0C0"
    SKYBLUE = "#87CEEB"
    SLATEBLUE = "#6A5ACD"
    SLATEGRAY = "#708090"
    SNOW = "#FFFAFA"
    SPRINGGREEN = "#00FF7F"
    STEELBLUE = "#4682B4"
    TAN = "#D2B48C"
    TEAL = "#008080"
    THISTLE = "#D8BFD8"
    TOMATO = "#FD6347"
    TURQUOISE = "#40E0D0"
    VIOLET = "#EE82EE"
    WHEAT = "#F5DEAA"
    WHITE = "#FFFFFF"
    WHITESMOKE = "#F5F5F5"
    YELLOW = "#FFFF00"
    YELLOWGREEN = "#9ACD32"


color = Color("HEX")  # have hex be the default
