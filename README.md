GASP (Graphics API for Students of Python)
==========================================

This version of GASP is built on Tkinter graphics, and unlike the other
versions, it was not written from scratch but was instead derived directly
from the livewires library, and is thus released under the same free software
license covering the original source:

  https://github.com/livewires/python

This version has been ported to Python 3.

Like all versions it is designed to enable absolute beginners to write 1980's
style arcade games as an introduction to python.

Homepage: https://codeberg.org/GASP/GASP\_Tkinter 

There is an excellent coursebook <http://openbookproject.net/pybiblio/gasp/>
which goes over learning to use GASP in your own applications.

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

TESTS
-----

The `tests` directory contains tests for GASP. For API level functional tests,
from inside the same directory where this README file is located, run:

    $ python3 -m doctest tests/test_api.rst

To visually inspect the graphics redering, run:

    $ python3 tests/test_graphics.py


LICENSE
-------

This software is licensed under the [GPL License](LICENSE.txt) found in this
distribution.




#### REQUIREMENTS

GASP is designed to not require any further installations, so nothing beyond the Python Standard Library (<https://docs.python.org/3/library/index.html>) is needed




