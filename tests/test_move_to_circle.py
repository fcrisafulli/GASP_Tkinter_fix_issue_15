import time

from gasp import *

messages = ["Here is a circle", "Moving it once", "Again", "A third time"]
begin_graphics(800, 600)
text = Text(messages[0], (100, 500))
circle = Circle((400, 300), 50, filled=True)

for i in range(3):
    time.sleep(2)
    remove_from_screen(text)
    text = Text(messages[i+1], (100, 500))
    move_to(circle, (500, 300))

end_graphics()
